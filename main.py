from discord.ext import commands
from AntiScam import AntiScam

whitelist = [] # Aqui vas agregar las ID's que quieres omitirle al bot
logs_channel = [] # Aqui va el ID del canal donde guardaras los logs

bot = commands.Bot(command_prefix='>')
bot.remove_command('help') # Elimine esta línea si desea utilizar el comando de ayuda.

@bot.listen()
async def on_message(message):
    await AntiScam(message, bot = bot, whitelist = whitelist, muted_role='Muted', verified_role='Verificado', logs_channel=logs_channel) #Esto lo puedes modificar si quieres darle otro nombre a los roles

bot.run('')
